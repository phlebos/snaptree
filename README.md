# Snaptree

Bash script to duplicate mounted filesystem tree using LVM snapshots where possible.
Designed for use with linux backup tools bareos in my case.

Usage 

snaptree create

Creates a readonly version of the filesystem tree under /mnt/snaptree

Uses read only LVM snapshots. If not LVM eg. /boot then does a read only mount bind.

snaptree remove

unmounts everything and removes snapshots.

Requires free space , not occupied by volumes, on any Logical Volume Groups for snapshots.



I am not a bash expert, but it works for my purposes. With Centos 8.

If you know of a better approach to doing this for backups with snapshots please let me know.

Using this I can get a full backup that can be recovered by Relax and Recover, rear.



