#!/bin/bash

# Copyright (C) 2020 Lee Hornby

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

########## Functions

Get_mount_table()
{
#	echo "Table"
	blkid -o export | awk '{if($1~/^DEVNAME=/){sub(/^DEVNAME=/,"",$1); print $1}}' \
	| xargs -I {} findmnt -lno source,target {}  | while read line || [[ -n $line ]] 
	do 
		local templine="$line"
		local path=$(echo "$templine" | cut -d " " -f2)
		if [[ $path != /run/* ]];
		then
		  local dpthcount=$(pathdepth $path)
		  echo "$dpthcount : $line"
		fi
	done
#xargs returns 0 if all had paths and mounted if some didn't the 123 others indicate a bigger problem.
}

Filter_table()
{
	if [ $# -lt 1 ]
	then
	        echo " $0 : sort_table() bad parms count" 1>&2
	        return 1
	fi
	echo "$1" | grep "/mnt/snaptree" 

}

sort_table()
{
	if [ $# -lt 1 ]
	then
	        echo " $0 : sort_table() bad parms count" 1>&2
	        return 1
	fi
	echo "$1" | sort $2 -

}

Get_mounted_devs()
{
	if [ $# -lt 1 ]
	then
	        echo " $0 : Get_mounted_devs() bad parms count" 1>&2
	        return 1
	fi
	echo "$1" | cut -d " " -f3

#	blkid -o export | awk '{if($1~/^UUID/){print $i}}' | xargs -I {} findmnt -lno source,target,fstype,uuid {}
#	blkid -o export | awk '{if($1~/^DEVNAME=/){sub(/^DEVNAME=/,"",$1); print $1}}' | xargs -I {} findmnt -lno source {}
#	return $?
}

Get_mount_path()
{
	if [ $# -lt 1 ]
	then
	        echo " $0 : Get_mount_path() bad parms count" 1>&2
	        return 1
	fi
	echo "$smtable" | grep -w "$1" | cut -d " " -f4
# 	findmnt -lno target $1
}

is_lvm()
{
	if [ $# -lt 1 ]
	then
	        echo " $0 : is_lvm() bad parms count" 1>&2
	        return 1
	fi
	lvs --noheadings $1 &> /dev/null
	if [ $? -eq 0 ]
	then #is
		return 1
	else #isnt
		return 0
	fi
}

is_lvm_snapshot()
{
	if [ $# -lt 1 ]
	then
	        echo " $0 : is_lvm_snapshot() bad parms count" 1>&2
	        return 1
	fi
	local attr=$(lvs --noheadings -o lv_attr $1 2> /dev/null | xargs )
	local targettype=${attr:0:1}
#	echo $targettype
	if [ "$targettype" = "s" ]
	then #is
		return 1
	else #isnt
		return 0
	fi
}

createsnap()
{
	if [ $# -lt 1 ]
	then
	        echo " $0 : createsnap() bad parms count" 1>&2
	        return 1
	fi

	local splitstr=$(dmsetup splitname --columns --noheadings $1)
	local strarry
	IFS=":" read -a strarry <<< $splitstr
	local vgstr=$(basename ${strarry[0]})
	local lvstr=${strarry[1]}

	if [ -e "/dev/$vgstr/snap$lvstr" ]; then
	   echo "$0 : snapshot /dev/$vgstr/snap$lvstr exists" 1>&2
	   rval=1
	else
	   	lvcreate --extents 10%ORIGIN --permission r --snapshot --name snap$lvstr /dev/$vgstr/$lvstr 1>&2
		rval=$? 
		if [ $? -eq 0 ]; then echo "/dev/$vgstr/snap$lvstr" ; fi
#		echo "$0 : lvcreate --extents 10%ORIGIN --permission r --snapshot --name \
#		snap$lvstr /dev/$vgstr/$lvstr" 1>&2
	fi
	return $rval
}

pathdepth()
{
	if [ $# -lt 1 ]
	then
	        echo " $0 : pathdepth() bad parms count" 1>&2
	        return 1
	fi
	if [ "$1" = "/" ]; then echo -n "0" 
	else
	echo "$1" | grep -o / | wc -l | tr -d '\n'
	fi 
}

mountsnap()
{
	if [ $# -lt 2 ]
	then
	        echo " $0 : mountsnap() bad parms count" 1>&2
	        return 1
	fi
#	echo $1 $2
	if [ ! -e "$1" ]; then
	{
		echo $0 : no snapshot $1
		return 1
#	test=1
	}
	fi

	local mountpath=$(Get_mount_path $2)

	echo "$0: mount --options defaults,nouuid,ro $1 /mnt/snaptree${mountpath}" 1>&2
	mount --options defaults,nouuid,,norecovery,ro $1 /mnt/snaptree${mountpath} 1>&2
}

bindmount()
{
	if [ $# -lt 1 ]
	then
	        echo " $0 : bindmount() bad parms count" 1>&2
	        return 1
	fi
	
	local mountpath=$(Get_mount_path $1)
	echo "$0: mount --bind  $mountpath /mnt/snaptree${mountpath}" 1>&2
	echo "$0: mount -o remount,bind,ro $mountpath /mnt/snaptree$mountpath" 1>&2
	mount --bind  $mountpath /mnt/snaptree${mountpath} 1>&2
	mount -o remount,bind,ro $mountpath /mnt/snaptree$mountpath 1>&2
}

################# Main functions
create_and_mount()
{
mtable=$(Get_mount_table)
ftable=$(Filter_table "$mtable")
if [ ! "$ftable" = "" ]; then echo "Snaptree is not clean, Do nothing"; exit 1 ; fi 
smtable=$(sort_table "$mtable")
echo "$smtable"

ftable=$(Filter_table "$mtable")

devs=$(Get_mounted_devs "$smtable") 

mkdir -p /mnt/snaptree

for  dev in $devs
do 
	is_lvm $dev
	if [ $? -eq 1 ]
	then #is
		is_lvm_snapshot $dev
		if [ $? -eq 0 ] 
		then
#			echo "Device $dev is an LVM and not a snapshot will mount at $(Get_mount_path $dev)"
			snapdev=$(createsnap $dev)
			if [ $? -eq 0 ] ; then  mountsnap $snapdev $dev ; fi
		else
			echo "Device $dev is an LVM and a snapshot will not mount"
		fi

	else #isnt
		echo "Device $dev is not an LVM will bind at $(Get_mount_path "$dev")"
		bindmount $dev
	fi
done

}

unmount_and_remove()
{
mtable=$(Get_mount_table)
ftable=$(Filter_table "$mtable")
smtable=$(sort_table "$ftable" "-r")
echo "$smtable"

devs=$(Get_mounted_devs "$smtable") 


for  dev in $devs
do 
	is_lvm $dev
	if [ $? -eq 1 ]
	then #is
		is_lvm_snapshot $dev
		if [ $? -eq 0 ] 
		then
			echo "Device $dev is an LVM and not a snaphot. It should not be in snaptree. Do nothing" 1>&2

		else
			echo "Device $dev is an LVM and a snapshot will unmount" 
			umount $dev
			echo "lvremove --yes $dev" 
			lvremove --yes $dev
		fi

	else #isnt
		echo "Device $dev is not an LVM will will unmount bind $(Get_mount_path "$dev")"
		umount $(Get_mount_path $dev)

	fi
done
}		

########## Do stuff

case "$1" in
        create)
		create_and_mount

        ;;

        remove)
		unmount_and_remove

        ;;

        *)
            echo $"Usage: $0 {create|remove}"
            exit 1
 
esac







	



